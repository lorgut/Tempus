\documentclass[a4paper,DIV=14]{scrartcl}

% Inline external files in the .tex
\usepackage{filecontents}

% For the heatmaps, and also used by pgfplotstable
\usepackage{pgfplots}
\pgfplotsset{
  compat=1.5.1,
  filter discard warning=false
}

% For the tables
\usepackage{ifthen}
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{pgfplotstable}
\pgfplotstableset{
  begin table=\begin{longtable},
  end table=\end{longtable},
  every head row/.style={before row=\toprule, after row=\midrule\endhead},
  every last row/.style={after row=\bottomrule}
}

\title{FOM fitness report for SnifferCongestionCovPhase1.yaml}
\author{GAmera ${project.version} (${timestamp})}
\date{\today}

\begin{document}
\begin{filecontents}{analysis-results.csv}
operator locations maxrange
PNR 0 1
POM 0 2
PRE 1 1
POC 1 1
PGR 0 1
PFP 1 1
WLM 0 2
WTM 0 2
WBL 0 1
WBT 0 1
RLO 1 1
RTU 0 4
RAF 0 15
RAO 1 4
RRO 5 5
RNO 1 2
RJR 0 4
RBR 0 3
RGR 0 2
RNW 0 1
ROM 0 2
ROS 0 1
RLM 0 2
RLA 0 4
RAW 0 1
RWB 0 1
RRR1 0 1
RRR2 0 2
RSR1 0 6
RSR2 0 21
RSR3 0 1
RSC 4 5
IRC 3 1
INC 3 1
\end{filecontents}
\begin{filecontents}{normalized-fitnesses.csv}
operator location attribute fitness strong
PRE 1 1 -37.0 0
POC 1 1 -37.0 0
PFP 1 1 -37.0 0
RLO 1 1 -37.0 0
RAO 1 1 -37.0 0
RAO 1 2 -37.0 0
RAO 1 3 -37.0 0
RAO 1 4 -37.0 0
RRO 1 1 -37.0 0
RRO 1 2 -37.0 0
RRO 1 3 -37.0 0
RRO 1 4 -37.0 0
RRO 1 5 -37.0 0
RRO 2 1 -37.0 0
RRO 2 2 -37.0 0
RRO 2 3 -37.0 0
RRO 2 4 -37.0 0
RRO 2 5 -37.0 0
RRO 3 1 -37.0 0
RRO 3 2 -37.0 0
RRO 3 3 -37.0 0
RRO 3 4 12.0 0
RRO 3 5 -37.0 0
RRO 4 1 -37.0 0
RRO 4 2 -37.0 0
RRO 4 3 -37.0 0
RRO 4 4 -37.0 0
RRO 4 5 -37.0 0
RRO 5 1 -37.0 0
RRO 5 2 12.0 0
RRO 5 3 -37.0 0
RRO 5 4 -37.0 0
RRO 5 5 12.0 0
RNO 1 1 -37.0 0
RNO 1 2 12.0 0
RSC 1 1 61.0 1
RSC 1 2 -37.0 0
RSC 1 3 61.0 1
RSC 1 4 -37.0 0
RSC 1 5 -37.0 0
RSC 2 1 61.0 1
RSC 2 2 61.0 1
RSC 2 3 -37.0 0
RSC 2 4 -37.0 0
RSC 2 5 61.0 1
RSC 3 1 61.0 1
RSC 3 2 -37.0 0
RSC 3 3 -37.0 0
RSC 3 4 -37.0 0
RSC 3 5 -37.0 0
RSC 4 1 -37.0 0
RSC 4 2 -37.0 0
RSC 4 3 61.0 1
RSC 4 4 -37.0 0
RSC 4 5 61.0 1
IRC 1 1 -37.0 0
IRC 2 1 -37.0 0
IRC 3 1 -37.0 0
INC 1 1 -37.0 0
INC 2 1 -37.0 0
INC 3 1 -37.0 0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-pre.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-poc.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-pfp.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-rlo.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-rao.csv}
dloc dattr dfit
1 1 -37.0
1 6 -37.0
1 11 -37.0
1 16 -37.0
1 20 -37.0

60 1 -37.0
60 6 -37.0
60 11 -37.0
60 16 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-rro.csv}
dloc dattr dfit
1 1 -37.0
1 5 -37.0
1 9 -37.0
1 13 -37.0
1 17 -37.0
1 20 -37.0

13 1 -37.0
13 5 -37.0
13 9 -37.0
13 13 -37.0
13 17 -37.0
13 20 -37.0

25 1 -37.0
25 5 -37.0
25 9 -37.0
25 13 12.0
25 17 -37.0
25 20 -37.0

37 1 -37.0
37 5 -37.0
37 9 -37.0
37 13 -37.0
37 17 -37.0
37 20 -37.0

49 1 -37.0
49 5 12.0
49 9 -37.0
49 13 -37.0
49 17 12.0
49 20 12.0

60 1 -37.0
60 5 12.0
60 9 -37.0
60 13 -37.0
60 17 12.0
60 20 12.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-rno.csv}
dloc dattr dfit
1 1 -37.0
1 11 12.0
1 20 12.0

60 1 -37.0
60 11 12.0
60 20 12.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-rsc.csv}
dloc dattr dfit
1 1 61.0
1 5 -37.0
1 9 61.0
1 13 -37.0
1 17 -37.0
1 20 -37.0

16 1 61.0
16 5 61.0
16 9 -37.0
16 13 -37.0
16 17 61.0
16 20 61.0

31 1 61.0
31 5 -37.0
31 9 -37.0
31 13 -37.0
31 17 -37.0
31 20 -37.0

46 1 -37.0
46 5 -37.0
46 9 61.0
46 13 -37.0
46 17 61.0
46 20 61.0

60 1 -37.0
60 5 -37.0
60 9 61.0
60 13 -37.0
60 17 61.0
60 20 61.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-irc.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

21 1 -37.0
21 20 -37.0

41 1 -37.0
41 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}
\begin{filecontents}{denormalized-fitnesses-inc.csv}
dloc dattr dfit
1 1 -37.0
1 20 -37.0

21 1 -37.0
21 20 -37.0

41 1 -37.0
41 20 -37.0

60 1 -37.0
60 20 -37.0
\end{filecontents}

\maketitle

\section{Analysis results}

\begin{center}
  \pgfplotstabletypeset[
    columns/operator/.style={column name={Operator}, string type},
    columns/locations/.style={column name={Locations}},
    columns/maxrange/.style={column name={Attribute}},
  ]{analysis-results.csv}
\end{center}

\section{Ranges}

\begin{description}
\item[Denormalized operator] From 1 to 10.
\item[Denormalized location] From 1 to 60.
\item[Denormalized attribute] From 1 to 20.
\item[Fitness] From 0 to 61.
\end{description}

\section{Strong mutants}

\pgfplotstableread{normalized-fitnesses.csv}\normfitnesses

\begin{center}
  \pgfplotstabletypeset[
    columns={operator,location,attribute,fitness},
    columns/operator/.style={column name={Operator}, string type},
    columns/location/.style={column name={Location}},
    columns/attribute/.style={column name={Attribute}},
    columns/fitness/.style={column name={Fitness},fixed},
    row predicate/.code={%
      \pgfplotstablegetelem{#1}{strong}\of{\normfitnesses}%
      \ifthenelse{\equal{\pgfplotsretval}{0}}{\pgfplotstableuserowfalse}{}%
    },
  ]{\normfitnesses}
\end{center}

\section{Fitness report}

\begin{center}
  \pgfplotstabletypeset[
    columns/operator/.style={column name={Operator}, string type},
    columns/location/.style={column name={Location}},
    columns/attribute/.style={column name={Attribute}},
    columns/fitness/.style={column name={Fitness},fixed},
    columns/strong/.style={
      column name={Strong?},
      string replace={0}{No},
      string replace={1}{Yes},
      string type
    },
  ]{\normfitnesses}
\end{center}

\section{Fitness heatmaps}

% \fitnessmap[axis options]{lowercase operator name}
\newcommand{\fitnessmap}[2][]{
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[
        title={Fitness heatmap for the \MakeUppercase{#2} operator},
        xlabel={Location},
        ylabel={Attribute},
        width=.6\textwidth,
        colorbar sampled,
        #1
        ]
        \addplot3[surf] table[x=dloc,y=dattr,z=dfit] {denormalized-fitnesses-#2.csv};
      \end{axis}
    \end{tikzpicture}
  \end{center}
}

\fitnessmap[view={0}{90}]{pre}

\fitnessmap[view={0}{90}]{poc}

\fitnessmap[view={0}{90}]{pfp}

\fitnessmap[view={0}{90}]{rlo}

\fitnessmap[view={0}{90}]{rao}

\fitnessmap[view={0}{90}]{rro}

\fitnessmap[view={0}{90}]{rno}

\fitnessmap[view={0}{90}]{rsc}

\fitnessmap[view={0}{90}]{irc}

\fitnessmap[view={0}{90}]{inc}


\end{document}
