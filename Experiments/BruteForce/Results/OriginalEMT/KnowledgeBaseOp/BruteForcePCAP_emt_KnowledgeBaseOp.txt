hof-0.30-acknowledgements 31.11
hof-0.30-administratively 36.36
hof-0.30-Australopithecus 31.11
hof-0.30-bloodthirstiness 32.86
hof-0.30-cinematographers 28.32
hof-0.30-counterespionage 27.27
hof-0.30-Czechoslovakians 36.36
hof-0.30-departmentalized 25.17
hof-0.30-disqualification 28.32
hof-0.30-enthusiastically 40.55
hof-0.30-flibbertigibbets 36.01
hof-0.30-gastrointestinal 34.96
hof-0.30-hospitalizations 32.16
hof-0.30-hydroelectricity 29.37
hof-0.30-intellectualized 33.21
hof-0.30-lightheartedness 27.97
hof-0.30-melodramatically 30.06
hof-0.30-multimillionaire 34.26
hof-0.30-nationalizations 30.06
hof-0.30-neurotransmitter 35.31
hof-0.30-overcompensating 30.41
hof-0.30-photojournalists 24.47
hof-0.30-predetermination 33.21
hof-0.30-rambunctiousness 27.62
hof-0.30-sentimentalizing 32.16
hof-0.30-synchronizations 31.81
hof-0.30-teleconferencing 37.76
hof-0.30-transcendentally 33.21
hof-0.30-unscrupulousness 31.11
hof-0.30-whatchamacallits 34.96
hof-0.45-acknowledgements 50.00
hof-0.45-administratively 49.30
hof-0.45-Australopithecus 45.80
hof-0.45-bloodthirstiness 48.25
hof-0.45-cinematographers 46.85
hof-0.45-counterespionage 43.00
hof-0.45-Czechoslovakians 49.65
hof-0.45-departmentalized 49.65
hof-0.45-disqualification 43.70
hof-0.45-enthusiastically 52.44
hof-0.45-flibbertigibbets 48.25
hof-0.45-gastrointestinal 47.90
hof-0.45-hospitalizations 50.69
hof-0.45-hydroelectricity 51.04
hof-0.45-intellectualized 52.79
hof-0.45-lightheartedness 44.05
hof-0.45-melodramatically 50.69
hof-0.45-multimillionaire 46.15
hof-0.45-nationalizations 45.80
hof-0.45-neurotransmitter 50.00
hof-0.45-overcompensating 44.40
hof-0.45-photojournalists 41.60
hof-0.45-predetermination 43.00
hof-0.45-rambunctiousness 50.00
hof-0.45-sentimentalizing 40.20
hof-0.45-synchronizations 50.69
hof-0.45-teleconferencing 54.54
hof-0.45-transcendentally 45.45
hof-0.45-unscrupulousness 46.50
hof-0.45-whatchamacallits 47.20
hof-0.60-acknowledgements 59.09
hof-0.60-administratively 59.79
hof-0.60-Australopithecus 62.23
hof-0.60-bloodthirstiness 68.88
hof-0.60-cinematographers 62.23
hof-0.60-counterespionage 64.33
hof-0.60-Czechoslovakians 67.48
hof-0.60-departmentalized 62.93
hof-0.60-disqualification 65.73
hof-0.60-enthusiastically 65.38
hof-0.60-flibbertigibbets 67.13
hof-0.60-gastrointestinal 64.68
hof-0.60-hospitalizations 59.09
hof-0.60-hydroelectricity 66.43
hof-0.60-intellectualized 63.98
hof-0.60-lightheartedness 57.34
hof-0.60-melodramatically 63.28
hof-0.60-multimillionaire 60.48
hof-0.60-nationalizations 59.79
hof-0.60-neurotransmitter 60.48
hof-0.60-overcompensating 66.43
hof-0.60-photojournalists 57.34
hof-0.60-predetermination 61.53
hof-0.60-rambunctiousness 61.53
hof-0.60-sentimentalizing 61.18
hof-0.60-synchronizations 67.83
hof-0.60-teleconferencing 66.43
hof-0.60-transcendentally 61.88
hof-0.60-unscrupulousness 62.58
hof-0.60-whatchamacallits 64.68
hof-0.75-acknowledgements 76.57
hof-0.75-administratively 70.62
hof-0.75-Australopithecus 73.07
hof-0.75-bloodthirstiness 77.97
hof-0.75-cinematographers 77.27
hof-0.75-counterespionage 79.02
hof-0.75-Czechoslovakians 79.02
hof-0.75-departmentalized 72.72
hof-0.75-disqualification 78.32
hof-0.75-enthusiastically 83.21
hof-0.75-flibbertigibbets 76.92
hof-0.75-gastrointestinal 80.76
hof-0.75-hospitalizations 75.87
hof-0.75-hydroelectricity 80.76
hof-0.75-intellectualized 78.32
hof-0.75-lightheartedness 76.92
hof-0.75-melodramatically 81.81
hof-0.75-multimillionaire 77.62
hof-0.75-nationalizations 76.57
hof-0.75-neurotransmitter 70.97
hof-0.75-overcompensating 76.57
hof-0.75-photojournalists 77.97
hof-0.75-predetermination 77.97
hof-0.75-rambunctiousness 79.02
hof-0.75-sentimentalizing 76.22
hof-0.75-synchronizations 76.92
hof-0.75-teleconferencing 76.57
hof-0.75-transcendentally 76.22
hof-0.75-unscrupulousness 75.87
hof-0.75-whatchamacallits 79.37
hof-0.90-acknowledgements 91.25
hof-0.90-administratively 93.35
hof-0.90-Australopithecus 91.95
hof-0.90-bloodthirstiness 89.16
hof-0.90-cinematographers 89.51
hof-0.90-counterespionage 89.51
hof-0.90-Czechoslovakians 92.65
hof-0.90-departmentalized 88.11
hof-0.90-disqualification 88.11
hof-0.90-enthusiastically 93.70
hof-0.90-flibbertigibbets 89.16
hof-0.90-gastrointestinal 91.95
hof-0.90-hospitalizations 88.81
hof-0.90-hydroelectricity 93.00
hof-0.90-intellectualized 91.25
hof-0.90-lightheartedness 90.90
hof-0.90-melodramatically 89.86
hof-0.90-multimillionaire 90.20
hof-0.90-nationalizations 91.95
hof-0.90-neurotransmitter 91.25
hof-0.90-overcompensating 88.81
hof-0.90-photojournalists 90.90
hof-0.90-predetermination 91.95
hof-0.90-rambunctiousness 91.60
hof-0.90-sentimentalizing 92.65
hof-0.90-synchronizations 92.30
hof-0.90-teleconferencing 89.16
hof-0.90-transcendentally 91.60
hof-0.90-unscrupulousness 90.90
hof-0.90-whatchamacallits 91.60
