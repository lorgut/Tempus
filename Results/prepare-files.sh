#!/bin/bash

copy_file() {
    cp ../Experiments/$1 $2
}

cd "$(dirname $(readlink -f "$0"))"

copy_file BruteForce/Results/BruteForce.ods BruteForce/BruteForce.ods
copy_file Island/Results/Island.ods IslandProgram/Island.ods
copy_file SnifferCongestion/Results/SnifferCongestion.ods SnifferCongestion/SnifferCongestion.ods
copy_file Terminal/Results/Terminal.ods TerminalProgram/Terminal.ods

find -name "*.csv.*" -delete
find -name "*.ods" -execdir ssconvert -S --export-type=Gnumeric_stf:stf_csv '{}' '{}'.csv \;
